﻿using System;
using System.Threading;

namespace GameEngine
{
    static class Program
    {
        static Configuration configuration;

        static void Main(string[] args)
        {
            try
            {
                configuration = new Configuration();
            }
            catch (Exception exception)
            {
                Log.WriteError("Configuration could not be initialized", exception: exception);
                return;
            }

            Console.ReadKey();
        }
    }
}
