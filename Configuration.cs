using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace GameEngine
{
    public class Configuration
    {
        // Stores default settings for the game engine - used when user has not specified
        // a specific setting
        public Dictionary<string, string> DefaultData { get; }

        // Stores user-specified settings for the game engine
        public Dictionary<string, string> UserData { get; private set; }

        // Path to configuration file
        private readonly string _filepath = Path.Combine(
            AppDomain.CurrentDomain.BaseDirectory,
            "configuration.json"
        );
        public string Filepath { get => _filepath; }

        /// Default constructor for Configuration class. Attempts to read configuration
        /// if it exists, and write configuration otherwise.
        public Configuration()
        {
            DefaultData = new Dictionary<string, string>() {
                {"video.width", "1280"},
                {"video.height", "720"}
            };
            UserData = new Dictionary<string, string>();

            if (File.Exists(Filepath))
            {
                Log.WriteLog("Configuration file found at " + Filepath);
                Read();
            }
            else
            {
                Log.WriteWarning("Configuration file not found at " + Filepath +
                    "; writing...");
                Write();
            }
        }

        // Reads Json file into temporary Dictionary<string, string>, then stores valid
        // key-value pairs in UserData dictionary
        public void Read()
        {
            var contents = File.ReadAllText(Filepath);
            var dictionary =
                JsonConvert.DeserializeObject<Dictionary<string, string>>(contents);

            foreach (var pair in dictionary)
            {
                if (DefaultData.ContainsKey(pair.Key))
                    UserData[pair.Key] = pair.Value;
                else
                    throw new Exception("Invalid key found in configuration: " +
                        pair.Key);
            }
        }

        // Serializes UserData dictionary into Json, and then writes configuration
        public void Write()
        {
            var serializedUserData = JsonConvert.SerializeObject(UserData);
            File.WriteAllText(Filepath, serializedUserData);
        }

        // Attempts to return value from UserData. If UserData does not contain key, will
        // return from DefaultData instead. If ignoreUserData is true, will return from
        // DefaultData instead.
        public string this[string key, bool ignoreUserData = false]
        {
            get
            {
                if (UserData.ContainsKey(key) && !ignoreUserData)
                    return UserData[key];
                return DefaultData[key];
            }
        }
    }
}