using System;
using System.Collections.Generic;

namespace GameEngine
{
    public static class Log
    {
        // Used for WriteLine to determine line color
        public enum Type
        {
            None,
            Success,
            Warning,
            Error
        }

        // Stores colors according to Log.Type enumerations
        static Dictionary<Type, ConsoleColor> TypeColor = new Dictionary<Type, ConsoleColor>() {
            {Type.None, ConsoleColor.Cyan},
            {Type.Success, ConsoleColor.Green},
            {Type.Warning, ConsoleColor.Yellow},
            {Type.Error, ConsoleColor.Red}
        };

        // Returns a pretty 24-hour timestamp. Used by WriteLine.
        public static string Timestamp()
        {
            DateTime time = DateTime.Now;
            return time.ToString("[HH:mm:ss] ");
        }

        public static void WriteLine(string message, Type type = Type.None, Exception exception = null, bool readKey = false)
        {
            Console.ForegroundColor = TypeColor[type];
            Console.Write(Timestamp());
            Console.WriteLine(message);
            if (exception != null)
                Console.WriteLine(exception);
            if (readKey)
                Console.ReadKey();
        }

        public static void WriteLog(string message) => WriteLine(message, type: Type.None, readKey: false);
        public static void WriteSuccess(string message) => WriteLine(message, type: Type.Success, readKey: false);
        public static void WriteWarning(string message) => WriteLine(message, type: Type.Warning, readKey: false);
        public static void WriteError(string message, Exception exception) =>
            WriteLine(message, type: Type.Error, exception: exception, readKey: true);
    }
}